package cs102;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class ButtonListener implements ActionListener {
    private ArrayList<JCheckBox> checkBoxes;

    public ButtonListener(ArrayList<JCheckBox> checkBoxes) {
        this.checkBoxes = checkBoxes;
    }

    public void actionPerformed(ActionEvent actionEvent) {
        String message = "Status:";

        for (JCheckBox checkBox : this.checkBoxes) {
            message += "," + checkBox.isSelected();
        }

        JOptionPane.showMessageDialog(null, message.replaceFirst(",", ""));
    }
}
